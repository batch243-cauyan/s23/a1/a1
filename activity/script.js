

// In the S23 folder, create an activity folder and an index.html and script.js file inside of it.
// Link the script.js file to the index.html file.
// Create a trainer object using object literals.
// Initialize/add the following trainer object properties:
// - Name (String)
// - Age (Number)
// - Pokemon (Array)
// - Friends (Object with Array values for properties)
// Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
// Access the trainer object properties using dot and square bracket notation.
// Invoke/call the trainer talk object method.
// Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the constructor)
// - Level (Provided as an argument to the consructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)
// Create/instantiate several pokemon object from the constructor with varying name and level properties.
// Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
// Create a faint method that will print out a message of targetPokemon has fainted.
// Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
// Invoke the tackle method of one pokemon object to see if it works as intended.
// Create a git repository named S23.
// Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
//  Add the link in Boodle.


let trainer = {
    name: 'Ash Ketchum',
    age: 10,
    pokemon : ["Pikachu", 'Charizard', 'Squirtle', 'Bulbasaur'],
    friends: {
        hoenn: ['May','Max'],
        kanto: ['Brock', 'Misty'],
    },
    talk: function(pokemonName){
        console.log("Result of talk method");
        console.log(`Pikachu! I choose you!`);
    }
}
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer['pokemon']);

trainer.talk();

function Pokemon(name, level){
    this.name = name;
    this.level= level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = function(pokemonTarget){
        let targetRemainingHealth = pokemonTarget.health - this.attack;
        pokemonTarget.health = targetRemainingHealth;
        console.log(`${this.name} tackled ${pokemonTarget.name}`)
        console.log(`${pokemonTarget.name}'s health has reduced to ${pokemonTarget.health}`);
        if (targetRemainingHealth <= 0){
            faint();
        }
        
        function faint(){
            console.log(`${pokemonTarget.name} has fainted.`);
        
         }
    }

}

let pikachu = new Pokemon('Pikachu', 12);
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);
let mewtwo = new Pokemon('Mewtwo', 100);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);

mewtwo.tackle(geodude);
console.log(geodude);

pikachu.tackle(mewtwo);